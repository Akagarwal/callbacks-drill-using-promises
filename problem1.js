const { errorMonitor } = require("events");
const fs = require("fs");
const path = require("path");



// Promise for creating a directory

function createDirctory(dir) {
  return new Promise((resolve, reject) => {
    fs.mkdir(dir, (data, err) => {
      if (err) {
        reject(err)
      } else {
        resolve('Directory created')
      }
    })
  })
}



// Promise for creating files

function createJson(dir) {
  return new Promise((resolve, reject) => {
    fs.writeFile(dir, "Hello", (err) => {
      if (err) {
        reject(err)
      } else {
        resolve('File is created')
      }
    })
  })
}



// Promise for deleting files

function deleteFiles(filePath) {
  let pathName = path.join(__dirname, filePath);
  let files = fs.readdirSync(pathName);
  let deleteFiles = files.map((eachfile) => {
    return new Promise((resolve, reject) => {
      let filePath = path.join(pathName, eachfile);
      fs.unlink(filePath, function (err) {
        if (err) {
          reject(err);
        } else {
          console.log('File deleted')
          resolve();
        }
      })
    })
  })
}



function problem1() {
  const dir = path.join(__dirname)
  createDirctory(dir + '/' + 'newDir')
    .then((data) => { console.log(data); return createJson(dir + '/newDir/' + 'file1.json') })
    .then((data) => { console.log(data); return createJson(dir + '/newDir/' + 'file2.json') })
    .then((data) => { console.log(data); return createJson(dir + '/newDir/' + 'file3.json') })
    .then((data) => { console.log(data); return createJson(dir + "/newDir/" + 'file4.json') })
    .then((data) => { console.log(data); return deleteFiles("newDir/") })
}

module.exports = problem1