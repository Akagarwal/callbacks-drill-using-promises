/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/


// Requiring Modules:

const fs = require('fs')
const path = require('path')


// Reading files from lipsum and converting data to upper funtion:

function readFile(dir){
    return new Promise((resolve, reject) => {

    fs.readFile(dir, "utf-8", (err, data)=>{
        if(err){
            reject(err)
        }else{
            console.log('File read successful')
            resolve(data)
            // const dataConverter = data.toString().toUpperCase()
        }
    })
})
}

// Writing files generic function:

function writeFile(dir, data){
    return new Promise((resolve, reject) => {

    fs.writeFile(dir, data, "utf-8", (err)=>{
        if(err){
            reject(err)
        }else{
            resolve('writing successful')
        }
    })
})
}


// Appending files genric function:

function appendFile(dir, data){
    return new Promise((resolve,reject) => {

        fs.appendFile(dir, data, (err) => {
            if(err){
                reject(err)
            }else{
                resolve('Appending successful')
            }
        })
    })
}


// Deleting files function

function deleteFiles(dir) {
    return new Promise((resolve, reject) => {
    let files = fs.readFile(dir, "utf-8", (err,data) => {
        if(err){
            console.log(err)
        }else{
            let arr = data.split("\n")
            // console.log(arr)
            arr.forEach((file) => {
                fs.unlink("../" + file, (err) => {
                    if (err){
                        reject(err)
                    }else{
                        resolve('Deleted files')
                    }
                })
            })
        }
    })
})
}


// Promise chain:

function problem2(){
    const dir = path.join(__dirname)
    readFile(dir + '/' + 'lipsum.txt')
    .then((data) => {const upperData = data.toUpperCase(); return writeFile(dir + '/' + 'lipsumUpper.txt', upperData)})
    .then((data) => {console.log(data); return writeFile(dir + '/' + 'filenames.txt', 'lipsumUpper.txt')})
    .then((data) => {console.log(data); return readFile(dir + '/' + 'lipsumUpper.txt')})
    .then((data) => {const lowerData = data.toLowerCase().split(".").join(".\n").toString(); return writeFile(dir + '/' + 'lipsumLower.txt', lowerData)})
    .then((data) => {console.log(data); return appendFile(dir + '/' + 'filenames.txt', "\n" + 'lipsumLower.txt')})
    .then((data) => {console.log(data); return readFile(dir + '/' + 'lipsumLower.txt')})
    .then((data) => {const sortData = data.split(".").sort().toString(); return writeFile(dir + '/' + 'lipsumSorted.txt', sortData)})
    .then((data) => {console.log(data); return appendFile(dir + '/' + 'filenames.txt', "\n" + 'lipsumSorted.txt')})
    .then((data) => {console.log(data); return deleteFiles(dir + '/' + 'filenames.txt')})
    .then((data) => {console.log(data)})
    .catch((err) => {console.log(err)})
  }
  
module.exports = problem2















// // require fs
// const fs = require('fs')
// const path = require('path')
// const { rejects } = require('assert')


// // Reading files from lipsum and converting data to upper funtion:

// function readFileLipsum(dir, callback){
//     fs.readFile(dir, "utf-8", (err, data)=>{
//         if(err){
//             console.log(err)
//         }else{
//             console.log('Reading Successful')
//             const dataConverter = data.toString().toUpperCase()
//             callback(dataConverter)
//         }
//     })
// }

// // Reading file from upper writing to upper generic write funtion:

// function readFileUpper(dir, callback){
//     fs.readFile(dir, "utf-8", (err, data)=>{
//         if(err){
//             console.log(err)
//         }else{
//             console.log('Reading Successful')
//             const lowerString = data.toString().toLowerCase().split(".").join(".\n")
//             const splitArray = lowerString.toString()
//             callback(splitArray)
//         }
//     })
// }


// // Reading file from lower writing to sort generic write funtion:

// function readFileLower(dir, callback){
//     fs.readFile(dir, "utf-8", (err, data)=>{
//         if(err){
//             console.log(err)
//         }else{
//             console.log('Reading Successful')
//             const dataArr = data.split(".").sort()
//             const dataStr = data.toString()
//             callback(dataStr)
//         }
//     })
// }



// // Writing files generic function:

// function writeFile(dir, data, callback){
//     fs.writeFile(dir, data, "utf-8", (err)=>{
//         if(err){
//             console.log(err)
//         }else{
//             console.log('writing successful')
//             callback()
//         }
//     })
// }


// // Writing filenames 

// function filenamesStore(dir, data, callback){
//     fs.appendFile(dir, data, (err) => {
//         if(err){
//             console.log(err)
//         }else{
//             console.log('Appending filenames successful')
//             callback()
//         }
//     }) 
// }


// // Deleting files

// function deleteFiles(dir) {
//     let files = fs.readFile(dir, "utf-8", (err,data) => {
//         if(err){
//             console.log(err)
//         }else{
//             console.log("successful")
//             let deleteFiles = files.map((eachfile) => {
//                 let filePath = path.join(pathName, eachfile);
//                 fs.unlink(filePath, function (err) {
//                   if (err) {
//                     console.log(err);
//                   } else {
//                     console.log("File deleted");
//                   }
//                 });
//               });
            
//         }
//     });
    
//   }



// // The main function:

// function problem2(){
//     const dir = path.join(__dirname);
//     readFileLipsum(dir + '/'+ 'lipsum.txt', (data) => {
//         writeFile(dir + '/' + 'lipsumUpper.txt', data, ()=>{
//             filenamesStore(dir + '/' + 'filenames.txt', dir + '/' + 'lipsumUpper.txt', () => {
//                 readFileUpper(dir + '/' + 'lipsumUpper.txt', (data) => {
//                     writeFile(dir + '/' + 'lipsumLower.txt', data, () => {
//                         filenamesStore(dir + '/' + 'filenames.txt', "\n" + dir + '/' + 'lipsumLower.txt', () => {
//                             readFileLower(dir + '/' + 'lipsumLower.txt', (data) => {
//                                 writeFile(dir + '/' + 'lipsumSorted.txt', data, () => {
//                                     filenamesStore(dir + '/' + 'filenames.txt', "\n" + dir + '/' + 'lipsumSorted.txt', () => {
//                                         deleteFiles(dir + '/' + 'filenames.txt')
//                                     })
//                                 })
//                             })
//                         })
//                     })
//                 })
//             })
//         })
//     })
// }
// problem2();
